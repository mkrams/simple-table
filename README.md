## Simple list

This project consists of a simple table which main purpose was to create one without using any framework 
and still have functionalities provided usually out of the box. These would be:
* Sorting by ASC/DESC clicking on header cell.
* Possibility to filter the column by implementing simple search input
* Table pagination

I chose React as my working environment, as it's something I'm most used to these days, but it could of course be also 
done in plain html, css and js. For creating the project, I used `create-react-app`.
As there are not so many elements, the two main components are just a Table and Pagination plus additional utilities module.
For the sake of tests I also had to add a little bit of babel configurations.
Linter is also added, but I took the rules for the config file from the web.
Styling is done with styled components. There is not much of it, so just simple css files could be used, but frankly I just 
find working with it more comfortable and tidier, so I went with it.

I like to have my work planned and see my progress "on paper", so I created a simple board on Trello for that purpose.
There's not that much tasks as it was just a table, but you can find it here: https://trello.com/b/QivgPhvd/simple-table
I left descriptions out as I found titles and checklists enough for me.

To run the app, please install necessary packages:

`npm install`

Then, start the project with:

`npm run start`

You can run tests with:

`npm run test`

To list out linter issues, use:

`npm run lint`

For development purposes, I also had a linter fixer script available:

`npm run linter-fix`