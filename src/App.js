import React from 'react';
import styled from 'styled-components';
import Table from './components/Table';

function App() {
    return (
        <AppWrapper className="App">
            <Table/>
        </AppWrapper>
    );
}

const AppWrapper = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export default App;