import { calculateIncomes, getRange } from '../Utils/utilities';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Utilitles', () => {
    it('Gets range from between two numbers', () => {
        const range = getRange(1, 5);
        expect(range).toEqual([ 1, 2, 3, 4, 5 ]);
    });
    it('Calculates incomes based on array of incomes and dates', () => {
        const mockDate = new Date();
        const presentMonth = mockDate.getMonth();
        const lastMonthMockDate = mockDate.setMonth(presentMonth - 1);

        const mockIncomes = [
            {
                'value': '3139.07',
                'date': '2019-12-06T22:50:45.645Z'
            },
            {
                'value': '2852.97',
                'date': '2019-05-03T19:09:59.087Z'
            },
            {
                'value': '8847.53',
                'date': '2019-09-17T19:39:40.561Z'
            },
            {
                'value': '4265.96',
                'date': '2019-02-03T16:14:44.419Z'
            },
            {
                'value': '2947.62',
                'date': '2019-06-07T19:24:08.843Z'
            },
            {
                'value': '4640.49',
                'date': '2019-02-23T20:55:26.059Z'
            },
            {
                'value': '4640.49',
                'date': lastMonthMockDate
            }
        ];

        const calculatedIncomes = calculateIncomes(mockIncomes);

        expect(calculatedIncomes.totalIncome).toEqual(31334.13);
        expect(calculatedIncomes.averageIncome).toEqual(4476.3);
        expect(calculatedIncomes.lastMonthIncome).toEqual(4640.49);
    });
});