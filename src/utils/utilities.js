const getTotalIncome = incomes => incomes.reduce((prev, curr) => Number(prev) + Number(curr.value), 0).toFixed(2);

const getAverageIncome = incomes => incomes.reduce((prev, curr) => Number(prev) + Number(curr.value) / incomes.length, 0).toFixed(2);

const getLastMonthIncome = incomes => {
    const lastMonth = new Date().getMonth() - 1;
    const lastMonthIncomes = incomes.filter(income => new Date(income.date).getMonth() === lastMonth);

    return getTotalIncome(lastMonthIncomes);
};

const calculateIncomes = incomes => {
    return {
        totalIncome: Number(getTotalIncome(incomes)),
        averageIncome: Number(getAverageIncome(incomes)),
        lastMonthIncome: Number(getLastMonthIncome(incomes))
    }
};

const getRange = (from, to, step = 1) => {
    const range = [];

    while (from <= to) {
        range.push(from);
        from += step;
    }

    return range;
};

module.exports = {
    calculateIncomes,
    getRange
};