import React from 'react';
import styled from 'styled-components';
import {calculateIncomes} from '../utils/utilities';
import Pagination from './Pagination';

class Table extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            companies: [],
            filteredCompanies: [],
            currentCompanies: [],
            filterOn: false,
            currentPage: null,
            totalPages: null,
            recordsPerPage: 30,
            sorted: {},
        }
    }

    componentDidMount() {
        this.getCompanies('https://recruitment.hal.skygate.io/companies');
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.filteredCompanies !== this.state.filteredCompanies) {
            this.handlePagination({
                currentPage: 1,
                totalPages: this.state.totalPages,
                totalRecordsValue: this.state.filteredCompanies.length,
                recordsPerPage: this.state.recordsPerPage,
            });
        }
    }

    // Gets data from state or localStorage if available.
    // If there's no data for companies in localStorage, it sets it there.
    getCompanies = async url => {
        if (localStorage.getItem('companies')) {
            this.setState({
                companies: JSON.parse(localStorage.getItem('companies')),
            });
        } else {
            const companies = await this.prepareCompaniesData(url);

            this.setState({
                companies: companies.sort((a, b) => a.id - b.id),
            });

            localStorage.setItem('companies', JSON.stringify(companies));
        }
    };

    // Gets list of companies and fills its incomes details
    prepareCompaniesData = async url => {
        const response = await fetch(url);
        const companies = await response.json();

        for (const company of companies) {
            try {
                const response = await fetch(`https://recruitment.hal.skygate.io/incomes/${company.id}`);
                const companyDetails = await response.json();

                company[ 'incomes' ] = calculateIncomes(companyDetails.incomes);
            } catch (err) {
                console.log('An error occurred during fetching:', err)
            }
        }

        return companies;
    };

    filterCompanies = event => {
        const filterValue = event.target.value.toUpperCase();
        const companies = this.state.companies;

        const filteredCompanies = companies.filter(company => company.name.toUpperCase().indexOf(filterValue) > -1);

        this.setState({
            filteredCompanies: filteredCompanies,
            filterOn: Boolean(filterValue.length),
            totalPages: Math.ceil(filteredCompanies.length / this.state.recordsPerPage),
        });
    };

    handlePagination = data => {
        const companies = this.state.filterOn ? this.state.filteredCompanies : this.state.companies;
        const {currentPage, totalPages, recordsPerPage} = data;

        const offset = (currentPage - 1) * recordsPerPage;
        const currentCompanies = companies.slice(offset, offset + recordsPerPage);

        this.setState({currentPage, currentCompanies, totalPages});
    };

    handleSorting = (sortBy) => {
        let sortedCompanies = [];
        const sorted = {};
        const companiesToSort = this.state.filterOn ? this.state.filteredCompanies : this.state.companies;

        if (!this.state.sorted[ `${sortBy}` ]) {
            if (sortBy === 'name' || sortBy === 'city') {
                sortedCompanies = companiesToSort.sort((a, b) => {
                    const textA = a[ `${sortBy}` ].toUpperCase();
                    const textB = b[ `${sortBy}` ].toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                sortedCompanies = companiesToSort.sort((a, b) => a.incomes[ `${sortBy}` ] - b.incomes[ `${sortBy}` ]);
            }

            sorted[ `${sortBy}` ] = true;
            this.setState({
                sorted
            });
        } else {
            sorted[ `${sortBy}` ] = false;
            sortedCompanies = companiesToSort.reverse();

            this.setState(() => ({
                sorted
            }))
        }

        this.setState({
            currentCompanies: sortedCompanies,
        });
    };

    render() {
        const {currentCompanies, companies} = this.state;

        return (
            <Wrapper>
                {companies.length ?
                    <>
                        <TopTableTools>
                            <InputFilter type="text" id="companyFilter" onKeyUp={this.filterCompanies}
                                         placeholder="Find company"/>
                            <Pagination totalRecordsValue={companies.length} recordsPerPage={30} pageNeighbours={1}
                                        handlePagination={this.handlePagination} totalPages={this.state.totalPages}/>
                        </TopTableTools>
                        <CompaniesTable>
                            <TableHead>
                                <tr>
                                    <HeaderCell onClick={() => this.handleSorting('id')}>ID</HeaderCell>
                                    <HeaderCell onClick={() => this.handleSorting('name')}>Name</HeaderCell>
                                    <HeaderCell onClick={() => this.handleSorting('city')}>City</HeaderCell>
                                    <HeaderCell onClick={() => this.handleSorting('totalIncome')}>Total
                                        income</HeaderCell>
                                    <HeaderCell onClick={() => this.handleSorting('averageIncome')}>Average
                                        income</HeaderCell>
                                    <HeaderCell onClick={() => this.handleSorting('lastMonthIncome')}>Last month
                                        income</HeaderCell>
                                </tr>
                            </TableHead>
                            <tbody>
                                {currentCompanies.map(company => (
                                    <TableRow key={company.id}>
                                        <StandardCell data-label="ID">{company.id}</StandardCell>
                                        <StandardCell data-label="Name">{company.name}</StandardCell>
                                        <StandardCell data-label="City">{company.city}</StandardCell>
                                        <StandardCell data-label="Total income">{company.incomes.totalIncome}</StandardCell>
                                        <StandardCell
                                        data-label="Average income">{company.incomes.averageIncome}</StandardCell>
                                        <StandardCell
                                        data-label="Last month income">{company.incomes.lastMonthIncome}</StandardCell>
                                    </TableRow>
                            ))}
                            </tbody>
                        </CompaniesTable>
                    </> :
                    <span>Fetching data...</span>}
            </Wrapper>
        )
    }
}

const Wrapper = styled.div`
    @media (max-width: 768px) {
      width: 100%;
    }
`;

const TopTableTools = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 10px 5px;
    margin: 5px 0;
`;

const CompaniesTable = styled.table`
    border: solid 1px #DDEEEE;
    border-collapse: collapse;
    border-spacing: 0;
    font: normal 13px Arial, sans-serif;
    @media (max-width: 768px) {
      display: flex;
      justify-content: center;
      padding-top: 20px;
      border: none;
    }
`;

const TableHead = styled.thead`
    background-color: #DDEFEF;
    border: solid 1px #DDEEEE;
    color: #336B6B;
    @media (max-width: 768px) {
        border: none;
        height: 0;
        overflow: hidden;
        position: absolute;
    }
`;

const HeaderCell = styled.th`
    padding: 10px;
    text-align: left;
    text-shadow: 1px 1px 1px #fff;
    cursor: pointer;
    &:hover {
        background-color: #A5B0B0;
    }
`;

const TableRow = styled.tr`
    @media (max-width: 768px) {
        border-bottom: 1px solid #ddd;
        display: block;
        margin-bottom: 5px;
    }
`;

const StandardCell = styled.td`
    border: solid 1px #DDEEEE;
    color: #333;
    padding: 10px;
    text-shadow: 1px 1px 1px #fff;
    @media (max-width: 768px) {
        display: block;
        font-size: 12px;
        text-align: right;
        :first-of-type {
            background-color: #DDEFEF;
        }
        ::before {
            content: attr(data-label);
            float: left;
            font-weight: bold;
        }
    }
`;

const InputFilter = styled.input`
    height: 25px;
    padding: 0 5px;
`;

export default Table;