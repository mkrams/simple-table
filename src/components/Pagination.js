import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { getRange } from '../utils/utilities';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

class Pagination extends React.Component {
    constructor(props) {
        super(props);
        const { totalRecordsValue = null, recordsPerPage = 30, neighbouringPages = 0, totalPages } = props;

        this.recordsPerPage = typeof recordsPerPage === 'number' ? recordsPerPage : 30;
        this.totalRecordsValue = typeof totalRecordsValue === 'number' ? totalRecordsValue : 0;

        // neighbouringPages can be: 0, 1 or 2
        this.neighbouringPages = typeof neighbouringPages === 'number'
            ? Math.max(0, Math.min(neighbouringPages, 2))
            : 0;

        this.totalPages = totalPages ? totalPages : Math.ceil(this.totalRecordsValue / this.recordsPerPage);

        this.state = { currentPage: 1 };
    }

    componentDidMount() {
        this.gotoPage(1);
    }

    gotoPage = page => {
        const { handlePagination = f => f } = this.props;

        const currentPage = Math.max(0, Math.min(page, this.totalPages));

        const paginationData = {
            currentPage,
            totalPages: this.totalPages,
            recordsPerPage: this.recordsPerPage,
            totalRecordsValue: this.totalRecordsValue
        };

        this.setState({ currentPage }, () => handlePagination(paginationData));
    };

    handleClick = page => evt => {
        evt.preventDefault();
        this.gotoPage(page);
    };

    handleMoveLeft = evt => {
        evt.preventDefault();
        this.gotoPage(this.state.currentPage - (this.neighbouringPages * 2) - 1);
    };

    handleMoveRight = evt => {
        evt.preventDefault();
        this.gotoPage(this.state.currentPage + (this.neighbouringPages * 2) + 1);
    };

    fetchPageNumbers = () => {
        const totalPages = this.props.totalPages;
        const currentPage = this.state.currentPage;
        const neighbouringPages = this.neighbouringPages;

        const totalNumbers = (this.neighbouringPages * 2) + 3;
        const totalBlocks = totalNumbers + 2;

        if (totalPages > totalBlocks) {
            const startPage = Math.max(2, currentPage - neighbouringPages);
            const endPage = Math.min(totalPages - 1, currentPage + neighbouringPages);

            let pages = getRange(startPage, endPage);

            const hasHiddenNumbersToRight = startPage > 2;
            const hasHiddenNumbersToLeft = (totalPages - endPage) > 1;
            const spillOffset = totalNumbers - (pages.length + 1);

            switch (true) {
                case (hasHiddenNumbersToRight && !hasHiddenNumbersToLeft): {
                    const extraPages = getRange(startPage - spillOffset, startPage - 1);
                    pages = [ LEFT_PAGE, ...extraPages, ...pages ];
                    break;
                }

                // handle: (1) {2 3} [4] {5 6} > (10)
                case (!hasHiddenNumbersToRight && hasHiddenNumbersToLeft): {
                    const extraPages = getRange(endPage + 1, endPage + spillOffset);
                    pages = [ ...pages, ...extraPages, RIGHT_PAGE ];
                    break;
                }

                // handle: (1) < {4 5} [6] {7 8} > (10)
                case (hasHiddenNumbersToRight && hasHiddenNumbersToLeft):
                default: {
                    pages = [ LEFT_PAGE, ...pages, RIGHT_PAGE ];
                    break;
                }
            }

            return [ 1, ...pages, totalPages ];

        }

        return getRange(1, totalPages);

    };

    render() {
        if (!this.totalRecordsValue || this.props.totalPages === 1) return null;

        const { currentPage } = this.state;
        const pages = this.fetchPageNumbers();

        return (
            <>
                <nav>
                    <PaginationWrapper>
                        {pages.map((page, index) => {
                            if (page === LEFT_PAGE) return (
                                <PageItem key={ index }>
                                    <PageLink href="/#" onClick={ this.handleMoveLeft }>
                                        <span>&laquo;</span>
                                    </PageLink>
                                </PageItem>
                            );

                            if (page === RIGHT_PAGE) return (
                                <PageItem>
                                    <PageLink href="/#" onClick={ this.handleMoveRight }>
                                        <span>&raquo;</span>
                                    </PageLink>
                                </PageItem>
                            );

                            return (
                                <PageItem key={ index } className={ `${ currentPage === page ? ' active' : '' }` }>
                                    <PageLink href="/#"
                                              onClick={ this.handleClick(page) }>{page}</PageLink>
                                </PageItem>
                            );
                        })}
                    </PaginationWrapper>
                </nav>
            </>
        );

    }
}

Pagination.propTypes = {
    totalRecordsValue: PropTypes.number.isRequired,
    recordsPerPage: PropTypes.number,
    neighbouringPages: PropTypes.number,
    totalPages: PropTypes.number,
    handlePagination: PropTypes.func
};

const PaginationWrapper = styled.ul`
  margin-top: 0;
  margin-bottom: 0;
  display: inline-block;
  padding: 0;
  .active > a {
    background-color: #6e98bb;
    color: #fff;
    border: 1px solid #6e98bb;
  }
`;

const PageItem = styled.li`
  display: inline;
`;

const PageLink = styled.a`
  color: #337ab7;
  padding: 5px 15px;
  font-size: 1rem;
  text-decoration: none;
  border: 1px solid #ddd;
`;

export default Pagination;